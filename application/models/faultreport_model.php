<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Model for the FAULT REPORT APPLICATION
| -------------------------------------------------------------------
| This file will contain all the functions to connect with the 
| Database relevant to the fault report application.
*/


class faultreport_model extends Model {

/**
 *	GET MY FAULTS
 *  @access Public
 *  @param 	string
 *				date 	=> date
 *  @param 	string
 *				uid 		=> end date and time
 *  @param 	string
 *				active  	=> if it is active or not.
 *  @param 	string	
 *				cid 		=> category id.	
 *  @param	string
 *				priority	=> filter by priority	
 *  @param	array
 *				order	=> array of order statements.		
 *  @return array
 *				return array of all the events.
 */
function get_faults($uid = false,$active = 1,$cid = false,$priority = false,$date = false,$order = false,$status = false)
{
	if (isset($date))
	{
		$date = date('Y-m-d H:i:s');
	}
	
	// Select the fields needed.
	$this->db->select('faultreport_faults.id, system_users.title, LEFT(system_users.first_name,1), system_users.last_name, faultreport_category.name, max(faultreport_messages.sent_date), max(faultreport_log.date), faultreport_faults.report_time, faultreport_faults.assigned, faultreport_faults.priority, faultreport_faults.status, faultreport_faults.deadline');
	
	// Join the tables that are needed. 
	$this->db->join('system_users','system_users.id=faultreport_faults.uid');  				// Join the users table with the faults table
	$this->db->join('faultreport_category','faultreport_category.id=faultreport_faults.cid'); 	// Join the faults table with the categories table.
	$this->db->join('faultreport_logs','faultreport_logs.fid=faultreport_faults.id');	// Join the faults table with the fault logs table.
	$this->db->join('faultreport_messages','faultreport_messages.fid=faultreport_faults.id');	// join the faults table with the message table.
	
	// Set the where clause.
	if (isset($date))
	{
		$this->db->where('faultreport_faults.report_time >=',$date);
	}
	if ($uid)
	{
		$this->db->where('faultreport_faults.uid',$uid);
	}
	if ($active != '')
	{
		$this->db->where('faultreport_faults.active',$active);
	}
	if ($cid)
	{
		$this->db->where('faultreport_faults.cid',$cid);
	}
	if ($priority)
	{
		$this->db->where('faultreport_faults.priority',$priority);
	}
	if ($status)
	{
		$this->db->where('faultreport_faults.status',$status);
	}
	
	// Set the group by
	$this->db->group_by('faultreport_faults.id');
	
	// Set the order
	if (is_array($order))
	{
		foreach ($order as $o)
		{
			$this->db->order_by($o['f'],$o['o']);
		}
	
	}
	else
	{
		$this->db->order_by('deadline','desc');
		$this->db->order_by('priority','asc');
		$this->db->order_by('report_time','desc');
	}
	
	$query = $this->db->get('faultreport_faults');
	
	if ($query->num_rows() > 0)
	{
		return $query->result();
	}
	else
	{
		return false;
	}
		
}


/**
 *	GET CATEGORIES
 *  @access Public
 *  @param	string
 *				parent	=> parent category	
 *  @return array
 *				return array of all the events.
 */
function get_categories($parent = 0)
{
	// Get the fields required
	$this->db->select('id,name,icon');
	
	//  Set the where 
	$this->db->where('parent',$parent);
	
	$query = $this->db->get('faultreport_category');
	
	if ($query->num_rows() > 0)
	{
		return $query->result();
	}
	else
	{
		return false;
	}
}

/**
 *	GET QUESTIONS
 *  @access Public
 *  @param	string
 *				parent	=> parent category	
 *  @return array
 *				return array of all the events.
 */
function get_questions($cid = 1)
{
	// Get the fields required
	$this->db->select('id,question,auto,editable,function');
	
	//  Set the where 
	$this->db->where('cid',$cid);
	
	$query = $this->db->get('faultreport_quesions');
	
	if ($query->num_rows() > 0)
	{
		return $query->result();
	}
	else
	{
		return false;
	}
}

/**
 *	GET LOG MESSAGES
 *  @access Public
 *  @param	string
 *				fid	=> fault id
 *  @return array
 *				return array of all the events.
 */
function get_logs($fid = false)
{
	if ($fid)
	{
		// Get the fields required
		$this->db->select('id,date,message,type,read');
		
		//  Set the where 
		$this->db->where('fid',$fid);
		
		$query = $this->db->get('faultreport_logs');
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}
}

/**
 *	GET MESSAGES
 *  @access Public
 *  @param	string
 *				fid	=> fault id
 *  @return array
 *				return array of all the events.
 */
function get_messages($fid = false)
{
	if ($fid)
	{
		// Get the fields required
		$this->db->select('id,message,type,sent_by,sent_date,read_date');
		
		//  Set the where 
		$this->db->where('fid',$fid);
		
		$query = $this->db->get('faultreport_messages');
		
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}
}

/**
 *	RECORD ANSWER
 *  @access Public
 *  @param	string
 *				fid	=> fault id
 *  @param	string
 *				qid	=> Question id
 *  @param	string
 *				answer	=> The Answer to the question
 *  @return boolean
 *				TRUE/FALSE
 */
function record_answer($fid = false,$qid = false,$answer = false)
{
	if ($fid && $qid)
	{
		// Store the data to be updated
		$record = array('fid'=>$fid,'qid'=>$qid,'answer'=>$answer);
		// Check to see if this is an update
		$this->db->where('qid',$qid);
		$this->db->where('fid',$fid);
		if ($this->db->count_all_results('faultreport_answers') == 0)
		{
			// No record currently exists so insert.
			$query = $this->db->insert('faultreport_answers',$record);
		}
		else
		{
			// Record already exists so update.
			$query = $this->db->update('faultreport_answers',$record,array('fid'=>$fid,'qid'=>$qid));
		}
		$this->db->flush_cache();

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

/**
 *	RECORD STATUS
 *  @access Public
 *  @param	string
 *				fid	=> fault id
 *  @param	string
 *				status	=> New Status
 *  @return boolean
 *				TRUE/FALSE
 */
function record_status($fid = false,$status = false)
{
	if ($fid && $status)
	{
		// Store the data to be updated
		$record = array('fid'=>$fid,'status'=>$status,'lastupdate'=>'now()');

		// Record already exists so update.
		$query = $this->db->update('faultreport_faults',$record,array('fid'=>$fid));

		// Flush the cache
		$this->db->flush_cache();

		// Record this update in the log
		$this->record_log($fid,'status','The status has been updated to '.$status,'now()');

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

/**
 *	RECORD LOG
 *  @access Public
 *  @param	string
 *				fid		=> fault id
 *  @param	string
 *				type	=> type of log message
 *  @param	string
 *				message	=> log message
 *  @param	string
 *				date	=> time of message
 *  @return boolean
 *				TRUE/FALSE
 */
function record_log($fid = false,$type = false,$message = false,$date = 'now()')
{
	if ($fid && $type && $message)
	{
		// Store the data to be updated
		$record = array('fid'=>$fid,'type'=>$type,'message'=>$message,'date'=>$date);

		// Record already exists so update.
		$query = $this->db->insert('faultreport_logs',$record);

		// Flush the cache
		$this->db->flush_cache();

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

/**
 *	RECORD Message
 *  @access Public
 *  @param	string
 *				fid		=> fault id
 *  @param	string
 *				sent_by	=> Message is sent by user id
 *  @param	string
 *				message	=> Message Detail
 *  @param	string
 *				sent_to	=> Message Sent to
 *  @param	string
 *				date	=> time of message
 *  @return boolean
 *				TRUE/FALSE
 */
function record_message($fid = false,$sent_by = false,$message = false,$sent_to = false,$date = 'now()')
{
	if ($fid && $message)
	{
		// Store the data to be updated
		$record = array('fid'=>$fid,'sent_by'=>$sent_by,'message'=>$message,'sent_to'=>$sent_to,'date'=>$date);

		// Record already exists so update.
		$query = $this->db->insert('faultreport_messages',$record);

		// Update the fault last updated status
		$this->update_fault($fid,'now()');

		// Flush the cache
		$this->db->flush_cache();

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

/**
 *	UPDATE last updated on fault
 *  @access Public
 *  @param	string
 *				fid			=> fault id
 *  @param	string
 *				lastupdated	=> Last Updated Time
 *  @return boolean
 *				TRUE/FALSE
 */
function update_fault($fid = false,$lastupdated = 'now()')
{
	if ($fid && $lastupdate)
	{
		// Store the data to be updated
		$record = array('fid'=>$fid,'lastupdated'=>$lastupdated);

		// Record already exists so update.
		$query = $this->db->update('faultreport_faults',$record,array('fid'=>$fid));

		// Flush the cache
		$this->db->flush_cache();

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

/**
 *	RECORD ASSIGNMENT CHANGE
 *  @access Public
 *  @param	string
 *				fid			=> fault id.
 *  @param	string
 *				assigned	=> User ID of the user this is assigned to.
 *  @return boolean
 *				TRUE/FALSE
 */
function record_assigned($fid = false,$assigned = false,$assign_name = false)
{
	if ($fid && $assigned)
	{
		// Store the data to be updated
		$record = array('assigned'=>$assigned,'lastupdated'=>'now()');

		// Record already exists so update.
		$query = $this->db->update('faultreport_faults',$record,array('fid'=>$fid));

		// Flush the cache
		$this->db->flush_cache();

		// Record this update in the log
		$this->record_log($fid,'assignment','The fault has been assigned to '.$assign_name,'now()');

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

/**
 *	RECORD FAULT
 *  @access Public
 *  @param	string
 *				fid	=> Fault ID of fault ID
 *  @param	string
 *				uid	=> User ID of fault owner
 *  @param	string
 *				cid	=> Category id
 *  @param	string
 *				report_time	=> Time of reporting the fault
 *  @param	string
 *				ip_address	=> IP Address
 *  @param	string
 *				machine	=> Machine ???
 *  @param	string
 *				status	=> Status of the fault NEw by default
 *  @param	string
 *				deadline	=> The deadline for the fault
 *  @param	string
 *				priority	=> Priority of the fault
 *  @return boolean
 *				TRUE/FALSE
 */
function record_fault($fid = false,$uid = false,$cid = false,$report_time = 'now()',$ip_address = $_SERVER['REMOTE_ADDR'],$machine = $_ENV["COMPUTERNAME"],$status = 'new',$deadline = false,$priority = false)
{
	if ($uid && $cid && $report_time && $ip_address && $status)
	{
		// Store the data to be updated
		$record = array('uid'=>$uid,'cid'=>$cid,'report_time'=>$report_time,'ip_address'=>$ip_address,'machine'=>$machine,'status'=>$status,'deadline'=>$deadline,'priority'=>$priority);
		
		// Check to see if this is an update
		if ($fid)
		{
			// Record already exists so update.
			$query = $this->db->update('faultreport_faults',$record,array('fid'=>$fid));
		}
		else
		{
			// No record currently exists so insert.
			$query = $this->db->insert('faultreport_faults',$record);
		}
		$this->db->flush_cache();

		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}
 
